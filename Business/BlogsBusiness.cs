using EFGetStartedV2.Data;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace EFGetStartedV2.Business
{
    public class BlogsBusiness
    {
        private readonly BloggingContext blogsRepository;
        public BlogsBusiness(BloggingContext blogsRepository)
        {
            this.blogsRepository = blogsRepository;
        }

        public async Task<Blog> Create(string url)
        {
            var newBlog = new Blog{Url=url};
            await blogsRepository.AddAsync(newBlog);
            await blogsRepository.SaveChangesAsync();
            return newBlog;
        }

        public async Task<bool> Delete(int id)
        {
            bool isDeleted = false;
            var blog = await blogsRepository.Blogs.FindAsync(id);
            if (blog != null)
            {
                blogsRepository.Remove(blog);
                await blogsRepository.SaveChangesAsync();
                isDeleted = true;
            }
            return isDeleted;
        }

        public async Task<Blog> GetById(int id)
        {
            var blog = await blogsRepository.Blogs.FindAsync(id);
            if (blog != null)
            {
                await blogsRepository.Entry(blog).Collection(b => b.Posts).LoadAsync();
            }
            return blog;
        }

        public async Task AddPost(int blogId, string title, string content, int authorId)
        {
            var blog = await blogsRepository.Blogs.FindAsync(blogId);
            var author = await blogsRepository.Authors.FindAsync(authorId);
            if (blog != null && author != null)
            {
                var newPost = new Post{
                    Blog=blog,
                    Author=author,
                    Title=title,
                    Content=content,
                    Date=DateTime.UtcNow
                };
                blog.Posts.Add(newPost);
                await blogsRepository.SaveChangesAsync();
            }
        }
    }
}