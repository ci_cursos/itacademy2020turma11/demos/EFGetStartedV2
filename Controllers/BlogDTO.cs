using System.Collections.Generic;

namespace EFGetStartedV2.Controllers
{
    public class BlogDTO
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public ICollection<dynamic> Posts {get;set;}
    }
}