﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using EFGetStartedV2.Business;
using EFGetStartedV2.Data;
using System.ComponentModel.DataAnnotations;

namespace EFGetStartedV2.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BlogsController : ControllerBase
    {

        private readonly ILogger<BlogsController> _logger;
        private readonly BlogsBusiness blogsBusiness;

        public BlogsController(ILogger<BlogsController> logger, BlogsBusiness blogsBusiness)
        {
            _logger = logger;
            this.blogsBusiness = blogsBusiness;
        }

        [HttpPut]
        //PUT /blogs
        public async Task<ActionResult<Blog>> Put([FromBody][Url][StringLength(200)]string url)
        {
            var newBlog = await blogsBusiness.Create(url);
            return newBlog;
        }

        [HttpGet("{id}")]
        //GET /blogs/{id}
        public async Task<ActionResult<BlogDTO>> GetById(int id)
        {
            var blog = await blogsBusiness.GetById(id);
            if (blog == null)
            {
                return NotFound();
            }
            var blogDto = new BlogDTO{
                Id = blog.BlogId,
                Url = blog.Url,
                Posts = blog.Posts.Select(p => new {p.Title, p.Content, p.Date}).ToList<dynamic>()
            };
            return blogDto;
        }

        [HttpPost("{id}/posts")]
        //POST /blogs/{id}/posts
        public async Task<ActionResult> AddPost(int id, [FromBody]PostDTO post)
        {
            var blog = await blogsBusiness.GetById(id);
            if (blog == null)
            {
                return NotFound();
            }
            await blogsBusiness.AddPost(id, post.Title, post.Content, post.AuthorId);
            return Ok();
        }
    }
}
