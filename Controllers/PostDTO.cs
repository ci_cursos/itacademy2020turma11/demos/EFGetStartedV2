using System.ComponentModel.DataAnnotations;

namespace EFGetStartedV2.Controllers
{
    public class PostDTO
    {
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Content { get; set; }
        [Required]
        public int AuthorId {get;set;}
    }
}