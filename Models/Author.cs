using System.ComponentModel.DataAnnotations;

namespace EFGetStartedV2.Data
{
    public class Author
    {
        public int AuthorId {get;set;}
        public string FirstName {get;set;}
        public string LastName {get;set;}
        [Required]
        public string Email {get;set;}
    }
}