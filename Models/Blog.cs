using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EFGetStartedV2.Data
{
    public class Blog
    {
        public int BlogId { get; set; }
        [Required]
        [MaxLength(200)]
        public string Url { get; set; }
        public List<Post> Posts { get; } = new List<Post>();
    }
}