using Microsoft.EntityFrameworkCore;

namespace EFGetStartedV2.Data
{
    public class BloggingContext : DbContext
    {
        public DbSet<Blog> Blogs {get; set;}
        public DbSet<Post> Posts {get;set;}
        public DbSet<Author> Authors {get;set;}

        public BloggingContext(){}
        public BloggingContext(DbContextOptions<BloggingContext> options)
        : base(options){}
        
    }
}