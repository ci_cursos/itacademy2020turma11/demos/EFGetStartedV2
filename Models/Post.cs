using System;
using System.ComponentModel.DataAnnotations;

namespace EFGetStartedV2.Data
{
    public class Post
    {
        public int PostId { get; set; }
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime Date {get; set;}
        public Blog Blog { get; set; }
        public int BlogId { get; set; }
        public Author Author {get;set;}
        public int AuthorId {get;set;}
    }
}